﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace HogwartSortingHat
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] ruleTextLines = File.ReadLines(@"C:\Temp\SortingHat\rules.txt").ToArray();
            Console.Out.WriteLine(string.Join(" | ", ruleTextLines));
            string[][] baseRules = ruleTextLines.Select(r => r.Split(": ")).ToArray();

            Dictionary<string, string> ruleset = new Dictionary<string, string>();
            foreach (var rulePair in baseRules)
            {
                ruleset.Add(rulePair[1], rulePair[0]);
            }

            Dictionary<string, List<string>> houses = new Dictionary<string, List<string>>();

            string[] students = File.ReadLines(@"C:\Temp\SortingHat\students.txt").ToArray();
            Console.Out.WriteLine(string.Join(" | ", students));

            foreach (var student in students)
            {
                if (student.Length > 20)    // LENGTH
                {
                    if (houses.ContainsKey(ruleset["length"]) == false)
                    {
                        List<string> startedHouse = new List<string>();
                        houses.Add(ruleset["length"], startedHouse);
                    }

                    List<string> selectedHouse = houses[ruleset["length"]];
                    selectedHouse.Add(student);
                }
                else if (new Regex("[aeiouy]", RegexOptions.IgnoreCase).
                    Match(student[0].ToString()).Success)    // VOWEL
                {
                    if (houses.ContainsKey(ruleset["vowel"]) == false)
                    {
                        List<string> startedHouse = new List<string>();
                        houses.Add(ruleset["vowel"], startedHouse);
                    }

                    List<string> selectedHouse = houses[ruleset["vowel"]];
                    selectedHouse.Add(student);
                }
                else if (student.Length % 2 == 0)       // EVEN
                {
                    if (houses.ContainsKey(ruleset["even"]) == false)
                    {
                        List<string> startedHouse = new List<string>();
                        houses.Add(ruleset["even"], startedHouse);
                    }

                    List<string> selectedHouse = houses[ruleset["even"]];
                    selectedHouse.Add(student);
                }
                else                                    // DEFAULT
                {
                    if (houses.ContainsKey(ruleset["default"]) == false)      
                    {
                        List<string> startedHouse = new List<string>();
                        houses.Add(ruleset["default"], startedHouse);
                    }

                    List<string> selectedHouse = houses[ruleset["default"]];
                    selectedHouse.Add(student);
                }
            }

            Console.Out.Write(System.Environment.NewLine + System.Environment.NewLine);

            foreach (string house in houses.Keys)
            {
                Console.Out.WriteLine(house + ": " + string.Join(", ", houses[house]));
            }
            

        }
    }
}
